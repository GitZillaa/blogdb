import React, { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';

const EditPost = ({ posts, updatePost }) => {
  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const { id } = useParams();
  const navigate = useNavigate();

  useEffect(() => {
    const post = posts.find(post => post.id === parseInt(id));
    if (post) {
      setTitle(post.title);
      setContent(post.content);
    }
  }, [id, posts]);  

  const handleSubmit = (e) => {
    e.preventDefault();
    updatePost({ id: parseInt(id), title, content });
    alert('Post Updated!');
    navigate('/');  
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Title:
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
      </label>
      <label>
        Content:
        <textarea value={content} onChange={(e) => setContent(e.target.value)} />
      </label>
      <button type="submit">Update</button>
    </form>
  );
};

export default EditPost;
