import React, { useState } from 'react';
import { Link } from 'react-router-dom';

const HomePage = () => {
  const [posts, setPosts] = useState([]);

  const deletePost = (id) => {
    if (window.confirm("Are you sure you want to delete this post?")) {
      setPosts(posts.filter(post => post.id !== id));
    }
  };

  return (
    <div>
      <h1>Blog Dashboard</h1>
      <Link to="/create">Create a New Post</Link>
      {posts.length > 0 ? (
        posts.map(post => (
          <div key={post.id}>
            <h2>{post.title}</h2>
            <p>{post.content}</p>
            <button onClick={() => deletePost(post.id)}>Delete</button>
            <Link to={`/edit/${post.id}`}>Edit</Link>
          </div>
        ))
      ) : (
        <p>No posts available. Click above to create one!</p>
      )}
    </div>
  );
};

export default HomePage;
