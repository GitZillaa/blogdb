import React, { useState } from 'react';
import axios from 'axios';

const CreatePost = () => {
  const [post, setPost] = useState({ title: '', content: '', id: '' });

  const handleChange = (e) => {
    setPost({...post, [e.target.name]: e.target.value});
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    axios.post('/edit/:id', post)
      .then(() => {
        alert('Post created successfully!');
        setPost({ title: '', content: '' }); // Reset form
      })
      .catch(error => {
        console.error('Failed to create post', error);
      });
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Title:
        <input type="text" name="title" value={post.title} onChange={handleChange} />
      </label>
      <label>
        Content:
        <textarea name="content" value={post.content} onChange={handleChange} />
      </label>
      <button type="submit">Create Post</button>
    </form>
  );
};

export default CreatePost;
